
import uproot
import numpy as np
import pandas as pd
import os
import pickle
from readroot import *


def bitConsistency(direc, pmt_mapType, Type, evtnum):
    # check the difference of each sample between pulse from raw waveform and ZS pod
    # --YQie, Feb. 2022
    
    even_map = "evenPOD.csv"
    odd_map = "oddPOD.csv"
    if(pmt_mapType=="even"):
        pmtRaw_map = even_map
    if(pmt_mapType=="odd"):
        pmtRaw_map = odd_map
    # Read Zero&Unzero suppressed mapping and select PMTs
    Channel_map = pd.read_csv(pmtRaw_map)
    POD_array = np.array(Channel_map['POD'])
    RAW_array = np.array(Channel_map['RAW'])
    
    PMTs = []
    if(Type=="OD"):
        PMTs = np.arange(800,921,1)
    if(Type=="TPC"):
        PMTs = list(np.arange(0,253,1))+list(np.arange(290,541,1))
         
    ZSPmtID_array = []
    RawID_array = []
    for ind in range(len(POD_array)):
        if(POD_array[ind] in PMTs and RAW_array[ind] in PMTs):
            ZSPmtID_array.append(POD_array[ind])
            RawID_array.append(RAW_array[ind])

     
    # Create arrays to store information about total events, total samples for each PMT
    consistency=np.zeros(len(ZSPmtID_array))
    evt_counter = np.zeros(len(ZSPmtID_array))
    sample_counter = np.zeros(len(ZSPmtID_array))
    evt_index = 0
    Warning=[]
    
    
    for filename in os.listdir(direc):
        if filename.endswith("raw.root"):
                fileIn = direc+"/"+filename
            
                file = uproot.open(fileIn)
            
                evt_map = evt_index_mapper(file)
                evt_ID = evt_map[:,0]
                
                # Extract various data from the data file
                evt_channels_arr = file['Data']['channel'].array(library='np')
                evt_pulse_start_times_arr = file['Data']['startTime'].array(library='np')
                evt_raw_pulse_array_arr = file['Data']['zData'].array(library='np')
                #truncatedPods = file['Data']['truncated'].array(library='np')
            
                for current_Evt in evt_ID:
                    
                    evt_index+=1
                    
                    # Run for specific number of events
                    if(evt_index > evtnum):
                                file.close()
                                return consistency,ZSPmtID_array,evt_counter,sample_counter,Warning
                    
                    # For current event, obtain pmt_ID, pulse array, start time for each pod and channel mapper
                    current_raw_pulse_array = raw_pulse_array(file, current_Evt, evt_raw_pulse_array_arr, evt_map)
                    current_pulse_timing_map = pulse_timing_map(file, current_Evt, evt_pulse_start_times_arr, evt_map)
                    current_ch_index_mapper = ch_index_mapper(file, current_Evt, evt_channels_arr, evt_map)
                    pmt_ID = np.unique(current_ch_index_mapper)
                    
                    # For each ZS pmt, decide whether it's in the pmt id list for this event
                    for i in range(len(ZSPmtID_array)):
                        
                        if(ZSPmtID_array[i] in pmt_ID):
                            
                            
                            PMT_index_pod = np.argwhere(current_ch_index_mapper == ZSPmtID_array[i])
                            PMT_index_raw = np.argwhere(current_ch_index_mapper == RawID_array[i])
                            
                            
                            current_PMT_trace_t0_raw = (current_pulse_timing_map[PMT_index_raw[0]])[0]
                            current_Pod_trace_raw = (current_raw_pulse_array[PMT_index_raw[0]])[0]
                            
                            # if there are more than one pod for the pmt, go through each one
                            if(len(PMT_index_pod)!=len(PMT_index_raw)):
                                
                                # Go through every pod in the pmt
                                for j in range(len(PMT_index_pod)):

                                    current_PMT_trace_t0_pod = (current_pulse_timing_map[PMT_index_pod[j]])[0]
                                    current_Pod_trace_pod = (current_raw_pulse_array[PMT_index_pod[j]])[0]
                                                                
                                    if(current_PMT_trace_t0_pod>current_PMT_trace_t0_raw):
                                    
                                        # Obtain part of the raw waveform which corresponds to the ZS pmt trace
                                        timeTraceDiff = current_PMT_trace_t0_pod-current_PMT_trace_t0_raw
                                        RawTracepart = current_Pod_trace_raw[int(timeTraceDiff):int(len(current_Pod_trace_pod)+timeTraceDiff)]
                                        
                                        # Consider the case when the raw waveform only cover part of the ZS pmt waveform
                                        if(len(RawTracepart)!=len(current_Pod_trace_pod)):
                                            current_Pod_trace_pod=current_Pod_trace_pod[:len(RawTracepart)]
                                        
                                        # Calculate the bit consistency for the pod
                                        TraceDiff = np.array(RawTracepart)-np.array(current_Pod_trace_pod)
                                        consistency[i]+=(abs(min(TraceDiff))+abs(max(TraceDiff)))
                                        evt_counter[i]+=1
                                        sample_counter[i]+=len(current_Pod_trace_pod)
                                        
                                        # If the bit consistency is nonzero, write a warning message directly
                                        if((abs(min(TraceDiff))+abs(max(TraceDiff)))!=0):
                                            print('Warning: NONZERO QUALITY FACTOR')
                                            print(current_Evt,ZSPmtID_array[i],PMT_index_pod[j],current_PMT_trace_t0_pod,RawID_array[i],PMT_index_raw[0],current_PMT_trace_t0_raw)
                                            Warning.append(str(current_Evt)+','+str(ZSPmtID_array[i])+','+str(PMT_index_pod[j][0])+','+str(current_PMT_trace_t0_pod))
                            
                            # For pmt that has only one pod
                            else:
                                
                                current_PMT_trace_t0_pod = (current_pulse_timing_map[PMT_index_pod[0]])[0]
                                current_Pod_trace_pod = (current_raw_pulse_array[PMT_index_pod[0]])[0]
                                
                            
                                if(current_PMT_trace_t0_pod>current_PMT_trace_t0_raw):
                                    
                                    timeTraceDiff = current_PMT_trace_t0_pod-current_PMT_trace_t0_raw
                                    
                                    RawTracepart = current_Pod_trace_raw[int(timeTraceDiff):int(len(current_Pod_trace_pod)+timeTraceDiff)]
                                    if(len(RawTracepart)!=len(current_Pod_trace_pod)):
                                            current_Pod_trace_pod=current_Pod_trace_pod[:len(RawTracepart)]
                                    TraceDiff = np.array(RawTracepart)-np.array(current_Pod_trace_pod)
                                    consistency[i]+=(abs(min(TraceDiff))+abs(max(TraceDiff)))
                                    evt_counter[i]+=1
                                    sample_counter[i]+=len(current_Pod_trace_pod)

                                    if((abs(min(TraceDiff))+abs(max(TraceDiff)))!=0):
                                            print('Warning: NONZERO QUALITY FACTOR')
                                            print(current_Evt,ZSPmtID_array[i],PMT_index_pod[j],current_PMT_trace_t0_pod,RawID_array[i],PMT_index_raw[0],current_PMT_trace_t0_raw)
                                            Warning.append(str(current_Evt)+','+str(ZSPmtID_array[i])+','+str(PMT_index_pod[0][0])+','+str(current_PMT_trace_t0_pod))
                                
                        
                        
    file.close()
    return consistency,ZSPmtID_array,evt_counter,sample_counter,Warning             




