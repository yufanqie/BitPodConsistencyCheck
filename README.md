# BitPodConsistencyCheck

Bit & pod consistency check is used to check whether zero & unzero suppressed pods match. 

In order to save memory space of DDC-32 boards, we only want to record part of the pulses which we are interested in instead of the entire raw waveform. Therefore, zero suppression (ZS) algorithm is implemented in FPGA. ZS test is used to check whether the ZS pods match the corresponding pulses from the raw waveform. Zero & Unzero suppressed mapping is used to obtain the pairs of LZ channel IDs. 

There are two types of ZS test: 

## Bit Consistency Check

For each event, each ZS pod contained in the event is obtained and compared with the pulse from corresponding raw waveform. The start of pulse in the raw waveform is determined by comparing the start time of ZS pod and that of raw waveform. The end of pulse is determined by the length of ZS pod. The difference between ZS pod and pulse from raw waveform is calculated, which represents the inconsistency. We expect it’s zero. 



## Pod Efficiency check

We simulate how FPGA identifies pulses. Each raw waveform is inverted and baseline subtracted. Then, each sample in the raw waveform is compared with the required threshold. When at least one sample crosses the threshold, there is a pod, which should be recorded and stored in the corresponding channel. For channel which has a crossing in the raw waveform but no corresponding ZS pod, this inconsistency is recored. 

There are several situations which may cause inconsistency:
The crossing is at exactly the threshold. The rounding of baseline used in Python is slightly difference from what FPGA does, so there is a inconsistency when the sample crossing the threshold is at exactly the threshold or one above. 
The start time of pulse in raw waveform is less than 5 samples away from the end time of the raw waveform, for which pulses cannot be recorded by ZS algorithm.
