#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Created on Thu May 28 18:17:30 2020
# Purpose: Store functions useful to development of a post-processing trigger
# module for DER data in LZ
# @author: mconveradmin

import numpy as np

def evt_index_mapper(file):    
    #Takes a file loaded from uproot using the uproot open function
    #and returns an array with three columns. 
    #The first column contains the evt id
    #The second contains the first pulse index for that event
    #The third contains the last pulse index for that event
    
    # create a dictionary, key: event id, value: [start index, end index], 2D array 
    evt_dict = {}
    for i, id in enumerate(file['Data']['evt'].array()):
        if id not in evt_dict:
            evt_dict[id] = [i, i]
        else:
            evt_dict[id][1] = i
    
    # create array with element of 3D array
    evt_map = np.zeros((len(evt_dict), 3))
    for i, (k, v) in enumerate(evt_dict.items()):
        evt_map[i][0] = k
        evt_map[i][1] = v[0]
        evt_map[i][2] = v[1]
    return evt_map


def ch_index_mapper(file,evt_ID,evt_channels_arr=[],evt_map=[]):    
    #Takes a file loaded from uproot using the uproot open function and an event id
    #Returns an array containing the channels of the pulses in the event
    if (len(evt_map) == 0): evt_map = evt_index_mapper(file )
    index_of_interest = np.argwhere(evt_map[:,0] == evt_ID)[0][0]
    start_index = int(evt_map[index_of_interest][1])
    end_index = int(evt_map[index_of_interest][2])
    if (len(evt_channels_arr) == 0):
        evt_channels = file['Data']['channel'].array()[start_index:end_index]
    else:
        evt_channels = evt_channels_arr[start_index:end_index]
    return evt_channels


def pulse_timing_map(file,evt_ID,evt_pulse_start_times_arr=[],evt_map=[]):
    #Takes a file loaded from uproot using the uproot open function and event id
    #Returns an array with the pulse start times
    if (len(evt_map) == 0): evt_map = evt_index_mapper(file)
    index_of_interest = np.argwhere(evt_map[:,0] == evt_ID)[0][0]
    start_index = int(evt_map[index_of_interest][1])
    end_index = int(evt_map[index_of_interest][2])
    if (len(evt_pulse_start_times_arr) == 0):
        evt_pulse_start_times = file['Data']['startTime'].array()[start_index:end_index]
    else:
        evt_pulse_start_times = evt_pulse_start_times_arr[start_index:end_index]
    return evt_pulse_start_times


def raw_pulse_array(file,evt_ID,evt_raw_pulse_array_arr=[],evt_map=[]):
    #Takes a file loaded from uproot using the uproot open function and event id
    #Returns an array with the raw pod pulses
    if (len(evt_map) == 0): evt_map = evt_index_mapper(file)
    index_of_interest = np.argwhere(evt_map[:,0] == evt_ID)[0][0]
    start_index = int(evt_map[index_of_interest][1])
    end_index = int(evt_map[index_of_interest][2])
    if (len(evt_raw_pulse_array_arr) == 0):
        evt_raw_pulse_array = file['Data']['zData'].array()[start_index:end_index]
    else:
        evt_raw_pulse_array = evt_raw_pulse_array_arr[start_index:end_index]
    return evt_raw_pulse_array



